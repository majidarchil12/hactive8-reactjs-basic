import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    username: '',
    password: '',
};

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        save: (state, action) => {
            return {
                ...state,
                ...action.payload,
            };
        },
        reset: () => initialState,
    },
});
