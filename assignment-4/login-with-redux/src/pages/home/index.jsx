import { Button, Form } from "react-bootstrap";
import { useAppDispatch, useAppSelector } from "../../redux/const";
import Swal from "sweetalert2";
import { authSlice } from "../../redux/auth/authSlice";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

export default function Home() {
    const dispatch = useAppDispatch()
    const auth = useAppSelector((state) => state.auth)
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault()

        Swal.fire({
            icon: 'success',
            title: 'Berhasil Logout',
            showConfirmButton: true,
            timer: 1000
        })

        setTimeout(() => {
            dispatch(authSlice.actions.reset())
            navigate('/')
        })
    }

    useEffect(() => {
        if (auth.username !== 'archil') {
            navigate('/')
        }
    }, [navigate, auth])

    return (
        <>
            <div className="d-flex flex-column min-vh-100 justify-content-center align-items-center">
                <p>
                    Selamat datang <b>{auth.username}</b>
                </p>
                <Form onSubmit={handleSubmit}>
                    <Button type="submit">
                        Logout
                    </Button>
                </Form>

            </div>
        </>
    )
}