import { useEffect, useReducer } from "react";
import { Button, FloatingLabel, Form, Image } from "react-bootstrap";
import { useAppDispatch, useAppSelector } from "../../redux/const";
import { authSlice } from "../../redux/auth/authSlice";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

export default function Login() {
    const dispatch = useAppDispatch()
    const [formState, setFormState] = useReducer(
        (state, newState) => ({
            ...state,
            ...newState,
        }),
        {
            username: 'archil',
            password: 'password',
        }
    );
    const auth = useAppSelector((state) => state.auth)
    const isLogin = Boolean(auth.username && auth.password);
    const navigate = useNavigate();

    const handleSubmit = (e) => {
        e.preventDefault()

        const { username, password } = formState;
        const checkLogin = username === 'archil' && password === 'password';

        if (checkLogin) {
            setTimeout(() => {
                dispatch(authSlice.actions.save(formState))

            }, 2000)

            Swal.fire({
                icon: 'success',
                title: 'Berhasil Login!',
                showConfirmButton: true,
                timer: 1000
            })

            navigate('/home')
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Gagal Login!',
                showConfirmButton: true,
                timer: 1000
            })
        }
    }

    useEffect(() => {
        if (isLogin) {
            navigate('/home')
        }
    }, [isLogin, navigate])

    return (
        <>
            <div className="d-flex flex-column min-vh-100 justify-content-center align-items-center">
                <Form onSubmit={handleSubmit} className="text-center">
                    <Image src="./logo192.png" className="my-3" />

                    <FloatingLabel
                        controlId="username"
                        label="Username"
                        className="mb-3"
                    >
                        <Form.Control type="text" name="username" placeholder="Username" value={formState.username} onChange={(e) => setFormState({ username: e.target.value })} />
                    </FloatingLabel>
                    <FloatingLabel controlId="password" label="Password">
                        <Form.Control type="password" name="password" placeholder="Password" value={formState.password} onChange={(e) => setFormState({ password: e.target.value })} />
                    </FloatingLabel>

                    <Button type="submit" className="mt-3">
                        Submit
                    </Button>
                </Form>
            </div>

        </>
    )
}