const arrayOfWords = ['cucumber', 'tomatos', 'avocado'];
const complicatedArray = ['cucumber', 44, true];

const checkString = (datas) => datas.every((data) => typeof data === "string")


const sortWords = (datas) =>
    new Promise((resolve, reject) => {
        if (!checkString(datas)) {
            return reject('data harus string!')
        } else {
            return resolve(datas.sort());
        }
    });

const makeAllCaps = (datas) =>
    new Promise((resolve, reject) => {
        if (!checkString(datas)) {
            return reject('data harus string!')
        } else {
            return resolve(
                datas.map((item) => item.toUpperCase())
            );
        }
    });

makeAllCaps(arrayOfWords)
    .then(sortWords)
    .then(result => { console.log(result) })
    .catch(error => { console.log(error) })

makeAllCaps(complicatedArray)
    .then(sortWords)
    .then(result => { console.log(result) })
    .catch(error => { console.log(error) })


