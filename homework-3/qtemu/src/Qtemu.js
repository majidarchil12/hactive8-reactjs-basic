import "bootstrap/dist/css/bootstrap.min.css";

function Qtemu() {
  return (
    <>
      <nav className="navbar navbar-expand-lg bg-body-tertiary">
        <div className="container-fluid">
          <a className="navbar-brand" href="/">
            QTemu
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarText"
            aria-controls="navbarText"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarText">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="/">
                  Create Meetup
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/">
                  Explore
                </a>
              </li>
            </ul>
            <span className="navbar-text">Login</span>
          </div>
        </div>
      </nav>
      <div className="container-fluid">

        <div className="row bg-secondary">
          <div className="col-2 my-3">
            <img src="./logo192.png" className="img-thumbnail my-3" alt="Logo" />
          </div>
          <div className="col-10 my-3">
            <h5>Hacktiv8 Metup</h5>
            <div className="row">
              <div className="col-1">
                <p>Location</p>
              </div>
              <div className="col-2">
                <p>Jakarta, Indonesia</p>
              </div>
            </div>
            <div className="row">
              <div className="col-1">
                <p>Members</p>
              </div>
              <div className="col-2">
                <p>1,078</p>
              </div>
            </div>
            <div className="row">
              <div className="col-1">
                <p>Organizers</p>
              </div>
              <div className="col-2">
                <p>Adhy Wiranata</p>
              </div>
            </div>
            <button type="button" className="btn btn-primary">Join Us</button>
          </div>
        </div>

        <div className="mt-3">
          <h4>Next Meetup</h4>
          <div className="row">
            <div className="col-11 bg-secondary mx-5">
              <h5>Awesome meetup and event</h5>
              <p className="text-muted">25 Januari 2019</p>
              <p className="fw-bold">
                Hello, Javascript & Node.js Ninjas! <br />
                Get ready for our monthly meetup JakartaJS! This will be our fifth meetup of 2018! <br />
                The Meetup format will contain some short stories and technical talks. <br />
                If you have a short announcement you'd like to share with the audience, you may do so during open mic announcements. <br /><br />
                Remember to bring a photo ID to get through building security.
                <br />
                - - - - - <br />
                See you there! <br />
                Best, Hengki, Giovanni, Sofian, Riza, Agung The JakartaJS Organizers
              </p>
            </div>
          </div>
        </div>

        <div className="mt-3">
          <div className="row">
            <div className="col-6">
              <div className="text-start">
                <h5>About Meetup</h5>
              </div>
            </div>
            <div className="col-6">
              <div className="text-end">
                <h5 >See all</h5>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-11 bg-secondary mx-5">
              <div className="row my-3">
                <div className="col-1">
                  <img src="./logo512.png" className="rounded-circle img-thumbnail" alt="Logo" />
                </div>
                <div className="col-11">
                  <h5>Organizers</h5>
                  <h5>Adhy Wiranata &emsp; 4 others.</h5>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="mt-3 mb-5">
          <div className="row">
            <div className="col-6 text-start">
              <h5>Past Meetups</h5>
            </div>
            <div className="col-6 text-end">
              <h5>See all</h5>
            </div>
          </div>

          <div className="container">
            <div className="row">
              <div className="col-4">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">27 November 2017</h5>
                    <hr />
                    <p className="card-text">
                      #39 JakartaJS April Meetup with kumparan
                    </p>
                    <p><b>139</b> went</p>
                    <a href="/" className="btn btn-primary">View</a>
                  </div>
                </div>
              </div>

              <div className="col-4">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">27 October 2017</h5>
                    <hr />
                    <p className="card-text">
                      #38 JakartaJS April Meetup with BliBli
                    </p>
                    <p><b>113</b> went</p>
                    <a href="/" className="btn btn-primary">View</a>
                  </div>
                </div>
              </div>

              <div className="col-4">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">27 September 2017</h5>
                    <hr />
                    <p className="card-text">
                      #37 JakartaJS April Meetup with Hacktiv8
                    </p>
                    <p><b>110</b> went</p>
                    <a href="/" className="btn btn-primary">View</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div className="container">
          <hr />
        </div>

        <footer>
          <p className="text-center my-5 fw-medium">Copyright Hacktiv8 2018</p>
        </footer>



      </div>
    </>
  );
}

export default Qtemu;
