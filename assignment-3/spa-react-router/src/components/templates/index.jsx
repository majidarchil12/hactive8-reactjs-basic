import { Container } from "react-bootstrap";
import Sidebar from "../sidebar";

export default function Template({ children, className }) {
    return (
        <>
            <main className='d-flex flex-nowrap vh-100'>
                <Sidebar className='vh-100' />
                <Container fluid className='vh-100'>
                    <Container>
                        <div className="d-flex align-items-center justify-content-center vh-100">
                            {children}
                        </div>
                    </Container>

                </Container>
            </main>
        </>
    )
}