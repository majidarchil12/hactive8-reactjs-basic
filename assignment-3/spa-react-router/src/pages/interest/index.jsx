import { Container } from "react-bootstrap";
import Template from "../../components/templates";

export default function Interest() {
    return (
        <>
            <Template>
                <Container fluid>
                    <h1>
                        Interest
                    </h1>
                    <hr />
                    <Container>
                        <p>
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque sequi suscipit deserunt? Ducimus ullam sed soluta numquam minima eos maxime dolorem? Nisi incidunt quaerat aspernatur commodi debitis sint facilis impedit.
                            Molestias nesciunt facilis voluptatibus veritatis doloremque repudiandae quas ad! Dolor quibusdam error necessitatibus sequi natus, eos vitae vel nesciunt, tempore nemo consequuntur. Nobis aliquid minima magnam beatae rerum nostrum velit.
                            Commodi, eveniet quaerat veniam qui sapiente ipsa modi distinctio ullam vitae quae exercitationem, consequatur porro, odit nisi in repellat nostrum repudiandae culpa ad eaque quam. Quod sint consequatur quas illum!
                        </p>
                    </Container>
                </Container>
            </Template>
        </>
    )
}