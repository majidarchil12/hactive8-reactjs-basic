import { Container } from "react-bootstrap";
import Template from "../../components/templates";

export default function About() {
    return (
        <>
            <Template>

                <Container fluid>
                    <h1>About Me</h1>
                    <hr />
                    <Container>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt earum sed distinctio doloremque velit eos ipsum cupiditate quia commodi expedita laborum, alias dolore esse deleniti non voluptatum a repellat quae?
                            Ad tenetur dicta nisi, veniam illo vitae adipisci beatae sint, quisquam eum, esse delectus! Optio veritatis itaque accusamus nihil culpa enim sequi labore? Cum non blanditiis, voluptates aspernatur quam ut?
                            Debitis, repellendus tenetur! Quibusdam odio voluptatum recusandae qui. Consequatur nobis nemo temporibus nesciunt voluptatum incidunt quam praesentium optio quo impedit at facere cum voluptate esse, aliquam corporis eius aut! Sit!
                        </p>
                    </Container>
                </Container>

            </Template >
        </>
    )
}