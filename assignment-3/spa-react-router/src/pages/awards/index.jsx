import { Col, Container, Row } from "react-bootstrap";
import Template from "../../components/templates";

export default function Awards() {
    return (
        <>
            <Template>
                <Container fluid>
                    <h1>Award</h1>
                    <hr />
                    <Container>
                        <Row>
                            <Col>
                                <dl>
                                    <dt>Runner Up NASA Hacking Competition</dt>
                                    <dd>
                                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident omnis quae debitis? Consequuntur doloribus praesentium provident adipisci reiciendis tenetur velit at hic expedita libero? Cum odio tempora ex dolores reprehenderit!
                                    </dd>
                                    <dt>#1 National Programming Contest</dt>
                                    <dd>
                                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident omnis quae debitis? Consequuntur doloribus praesentium provident adipisci reiciendis tenetur velit at hic expedita libero? Cum odio tempora ex dolores reprehenderit!
                                    </dd>
                                </dl>
                            </Col>

                            <Col>
                                <dl>
                                    <dt>Runner Up Google Pentest</dt>
                                    <dd>
                                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident omnis quae debitis? Consequuntur doloribus praesentium provident adipisci reiciendis tenetur velit at hic expedita libero? Cum odio tempora ex dolores reprehenderit!
                                    </dd>
                                    <dt>#1 Most Efficient Database Engineering</dt>
                                    <dd>
                                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident omnis quae debitis? Consequuntur doloribus praesentium provident adipisci reiciendis tenetur velit at hic expedita libero? Cum odio tempora ex dolores reprehenderit!
                                    </dd>
                                </dl>
                            </Col>
                        </Row>

                    </Container>
                </Container>
            </Template>
        </>
    )
}