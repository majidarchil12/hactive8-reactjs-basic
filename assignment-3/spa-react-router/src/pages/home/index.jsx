import { Container } from "react-bootstrap";
import Template from "../../components/templates";

export default function Home() {
    return (
        <>
            <Template>
                <Container fluid>
                    <h1>Archil Aulia Najahy Majid</h1>
                    <hr />
                    <Container>
                        <p className="text-wrap">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nihil, dolore earum, in tempora quasi dolorem ipsam fuga dolor sapiente nobis ullam eius aspernatur atque porro placeat itaque magni? Qui, impedit.
                            Blanditiis at quibusdam ex non laudantium iusto magni labore natus ullam tenetur voluptatem veniam nesciunt quod nam architecto itaque eveniet, alias dolor inventore suscipit! Quidem cumque doloremque similique minus ad.
                        </p>
                        <p>
                            <i className="bi bi-telephone me-2"></i>
                            <a className="btn btn-outline-primary" href="https://wa.me/62896123123123" target="__blank">+62896123123123
                            </a>
                        </p>
                        <p>
                            <i className="bi bi-envelope me-2"></i>
                            <a className="btn btn-outline-success" href="mailto:archil.majid@untan.ac.id">
                                archil.majid@untan.ac.id
                            </a>
                        </p>
                        <p>
                            <i className="bi bi-house me-2"></i>
                            <a className="btn btn-outline-info" href="https://maps.app.goo.gl/gJBdwjQeqCEiD61A8">
                                Universitas Tanjungpura
                            </a>
                        </p>
                    </Container>
                </Container>

            </Template>
        </>
    )
}