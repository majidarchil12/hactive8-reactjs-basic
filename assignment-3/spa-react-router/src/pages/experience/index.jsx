import { Col, Container, Row } from "react-bootstrap";
import Template from "../../components/templates";

export default function Experience() {
    return (
        <>
            <Template>
                <div className="container-fluid">
                    <h1>Experience</h1>
                    <hr />
                    <Container>
                        <Row>
                            <Col>
                                <ul>
                                    <li>
                                        Google Inc.
                                    </li>
                                    <li>
                                        Amazon
                                    </li>
                                    <li>
                                        Stark Corp
                                    </li>
                                </ul>
                            </Col>
                            <Col>
                                <ul>
                                    <li>
                                        Osborn Corp.
                                    </li>
                                    <li>
                                        Amazon
                                    </li>
                                    <li>
                                        Stark Corp
                                    </li>
                                </ul>
                            </Col>
                        </Row>

                    </Container>

                </div>

            </Template>
        </>
    )
}