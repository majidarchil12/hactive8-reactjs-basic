import { Col, Container, Row } from "react-bootstrap";
import Template from "../../components/templates";

export default function Skills() {
    return (
        <>
            <Template>
                <Container fluid>
                    <h1>Skills</h1>
                    <hr />
                    <Container>
                        <Row>
                            <Col>
                                <h3>
                                    <u>
                                        Programming Language
                                    </u>
                                </h3>

                                <h5>
                                    php
                                    <i className="bi bi-star-fill ms-2"></i>
                                    <i className="bi bi-star-fill"></i>
                                    <i className="bi bi-star-fill"></i>
                                    <i className="bi bi-star-fill"></i>
                                    <i className="bi bi-star"></i>
                                </h5>

                                <h5>
                                    javascript
                                    <i className="bi bi-star-fill ms-2"></i>
                                    <i className="bi bi-star-fill"></i>
                                    <i className="bi bi-star"></i>
                                    <i className="bi bi-star"></i>
                                    <i className="bi bi-star"></i>
                                </h5>

                            </Col>

                            <Col>
                                <h3>
                                    <u>
                                        Web Framework
                                    </u>
                                </h3>

                                <h5>
                                    Laravel
                                    <i className="bi bi-star-fill ms-2"></i>
                                    <i className="bi bi-star-fill"></i>
                                    <i className="bi bi-star-fill"></i>
                                    <i className="bi bi-star"></i>
                                    <i className="bi bi-star"></i>
                                </h5>

                                <h5>
                                    Next.js
                                    <i className="bi bi-star-fill ms-2"></i>
                                    <i className="bi bi-star"></i>
                                    <i className="bi bi-star"></i>
                                    <i className="bi bi-star"></i>
                                    <i className="bi bi-star"></i>
                                </h5>
                            </Col>
                        </Row>
                    </Container>
                </Container>
            </Template>
        </>
    )
}