import { createBrowserRouter } from "react-router-dom";
import Home from "../pages/home";
import About from "../pages/about";
import Experience from "../pages/experience";
import Skills from "../pages/skills";
import Interest from "../pages/interest";
import Awards from "../pages/awards";

export const router = createBrowserRouter([
    {
        path: "/",
        element: <Home />
    },
    {
        path: "/about",
        element: <About />
    },
    {
        path: "/experience",
        element: <Experience />
    },
    {
        path: "/skills",
        element: <Skills />
    },
    {
        path: "/interest",
        element: <Interest />
    },
    {
        path: "/awards",
        element: <Awards />
    }
])