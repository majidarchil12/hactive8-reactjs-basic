import { useEffect, useState } from 'react'

function Currency() {
    const [allCurrency, setAllCurrency] = useState([])
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        setIsLoading(true);
        fetch('https://api.currencyfreaks.com/v2.0/rates/latest?apikey=0c02c8d652724a0dbdcab75a290f3770')
            .then(response => response.json())
            .then(data => {
                setAllCurrency(data);
                setIsLoading(true);
            });
        console.log(allCurrency)
    }, [])

    return (
        <>
            <div className="container h-100">
                <div className="d-flex align-items-center justify-content-center vh-100">
                    <div className="table-responsive">
                        <table className="table text-center">
                            <thead>
                                <tr>
                                    <th>Currency</th>
                                    <th>We Buy</th>
                                    <th>Exchange Rate</th>
                                    <th>We Sell</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    allCurrency.length === 0 ? <> <tr><td colSpan={4}>Tidak ada data</td></tr> </> :
                                        <>
                                            <tr>
                                                <td>CAD</td>
                                                <td>{parseFloat(allCurrency.rates.CAD) + (parseFloat(allCurrency.rates.CAD) * (5 / 100))}</td>
                                                <td>{allCurrency.rates.CAD}</td>
                                                <td>{parseFloat(allCurrency.rates.CAD) - (parseFloat(allCurrency.rates.CAD) * (5 / 100))}</td>
                                            </tr>
                                            <tr>
                                                <td>EUR</td>
                                                <td>{parseFloat(allCurrency.rates.EUR) + (parseFloat(allCurrency.rates.EUR) * (5 / 100))}</td>
                                                <td>{allCurrency.rates.EUR}</td>
                                                <td>{parseFloat(allCurrency.rates.EUR) - (parseFloat(allCurrency.rates.EUR) * (5 / 100))}</td>
                                            </tr>
                                            <tr>
                                                <td>IDR</td>
                                                <td>{parseFloat(allCurrency.rates.IDR) + (parseFloat(allCurrency.rates.IDR) * (5 / 100))}</td>
                                                <td>{allCurrency.rates.IDR}</td>
                                                <td>{parseFloat(allCurrency.rates.IDR) - (parseFloat(allCurrency.rates.IDR) * (5 / 100))}</td>
                                            </tr>
                                            <tr>
                                                <td>JPY</td>
                                                <td>{parseFloat(allCurrency.rates.JPY) + (parseFloat(allCurrency.rates.JPY) * (5 / 100))}</td>
                                                <td>{allCurrency.rates.JPY}</td>
                                                <td>{parseFloat(allCurrency.rates.JPY) - (parseFloat(allCurrency.rates.JPY) * (5 / 100))}</td>
                                            </tr>
                                            <tr>
                                                <td>CHF</td>
                                                <td>{parseFloat(allCurrency.rates.CHF) + (parseFloat(allCurrency.rates.CHF) * (5 / 100))}</td>
                                                <td>{allCurrency.rates.CHF}</td>
                                                <td>{parseFloat(allCurrency.rates.CHF) - (parseFloat(allCurrency.rates.CHF) * (5 / 100))}</td>
                                            </tr>
                                            <tr>
                                                <td>GBP</td>
                                                <td>{parseFloat(allCurrency.rates.GBP) + (parseFloat(allCurrency.rates.GBP) * (5 / 100))}</td>
                                                <td>{allCurrency.rates.GBP}</td>
                                                <td>{parseFloat(allCurrency.rates.GBP) - (parseFloat(allCurrency.rates.GBP) * (5 / 100))}</td>
                                            </tr>
                                        </>
                                }


                            </tbody>
                        </table>

                        <p className='text-center text-white'> Rates are based from 1 USD <br />
                            This application uses API from&nbsp;
                            <a href="https://currencyfreaks.com/" className='text-white'>
                                https://currencyfreaks.com/
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Currency