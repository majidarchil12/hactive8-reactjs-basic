import AtomsButton from "../../atoms/button";
import AtomCol from "../../atoms/col";
import AtomRow from "../../atoms/row";
import MoleculeJumbotronData from "../../molecules/jumbotron-data";

export default function OrganismJumbotron() {
    return (
        <>

            <AtomRow className={'bg-secondary'}>
                <AtomCol column={2} className={'my-3'}>
                    <img src="./logo192.png" className="img-thumbnail my-3" alt="Logo" />
                </AtomCol>
                <AtomCol column={10} className={'my-3'}>
                    <h5>Hacktiv8 Metup</h5>
                    <MoleculeJumbotronData datas={['Location', 'Jakarta, Indonesia']} />

                    <MoleculeJumbotronData datas={['Members', '1,078']} />

                    <MoleculeJumbotronData datas={['Organizers', 'Adhy Wiranata']} />

                    <AtomsButton type={'button'} color={'primary'}>Join Us</AtomsButton>

                </AtomCol>
            </AtomRow>

        </>
    )
}