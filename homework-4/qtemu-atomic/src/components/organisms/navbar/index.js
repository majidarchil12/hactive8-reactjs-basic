import AtomNavLink from "../../atoms/link/nav-link";

export default function OrganismNavbar() {
    return (
        <>
            <nav className="navbar navbar-expand-lg bg-body-tertiary">
                <div className="container-fluid">
                    <a className="navbar-brand" href="/">
                        QTemu
                    </a>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarText"
                        aria-controls="navbarText"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarText">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <AtomNavLink route={'/'} >
                                    Create Meetup
                                </AtomNavLink>
                            </li>
                            <li className="nav-item">
                                <AtomNavLink route={'/'} >
                                    Explore
                                </AtomNavLink>
                            </li>
                        </ul>
                        <span className="navbar-text">Login</span>
                    </div>
                </div>
            </nav>
        </>
    )
}