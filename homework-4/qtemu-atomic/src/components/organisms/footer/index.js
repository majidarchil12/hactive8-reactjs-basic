export default function OrganismFooter() {
    return (
        <>
            <div className='container'>
                <hr />
                <footer>
                    <p className="text-center my-5 fw-medium">Copyright Hacktiv8 2018</p>
                </footer>
            </div>
        </>
    )
}