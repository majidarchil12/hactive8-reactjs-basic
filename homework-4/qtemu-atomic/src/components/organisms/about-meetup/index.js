import AtomCol from "../../atoms/col";
import AtomRow from "../../atoms/row";
import MoleculeHeaderContent from "../../molecules/header-content";

export default function OrganismAboutMeetup() {
    return (
        <>
            <div className="mt-3">
                <MoleculeHeaderContent title={'About Meetup'} />

                <AtomRow>
                    <AtomCol column={11} className="bg-secondary mx-5">
                        <AtomRow className="my-3">
                            <AtomCol column={1}>
                                <img src="./logo512.png" className="rounded-circle img-thumbnail" alt="Logo" />
                            </AtomCol>
                            <AtomCol column={11}>
                                <h5>Organizers</h5>
                                <h5>Adhy Wiranata &emsp; 4 others.</h5>
                            </AtomCol>
                        </AtomRow>
                    </AtomCol>
                </AtomRow>
            </div>
        </>
    )
}