import AtomCol from "../../atoms/col";
import AtomRow from "../../atoms/row";
import MoleculeCard from "../../molecules/card";
import MoleculeHeaderContent from "../../molecules/header-content";

export default function OrganismPastMeetups({ children }) {
    return (
        <>
            <div className="mt-3 mb-5">
                <MoleculeHeaderContent title={'Past Meetups'} />

                <div className="container">
                    <AtomRow>
                        <AtomCol column={4}>
                            <MoleculeCard title={'27 November 2017'} body={'#39 JakartaJS April Meetup with kumparan'} attendance={'139'} />
                        </AtomCol>
                        <AtomCol column={4}>
                            <MoleculeCard title={'27 October 2017'} body={'#38 JakartaJS April Meetup with BliBli'} attendance={'113'} />
                        </AtomCol>
                        <AtomCol column={4}>
                            <MoleculeCard title={'27 September 2017'} body={'#37 JakartaJS April Meetup with Hacktiv8'} attendance={'110'} />
                        </AtomCol>
                    </AtomRow>
                </div>
            </div>
        </>
    )
}