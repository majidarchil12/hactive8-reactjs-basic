import AtomCol from "../../atoms/col";
import AtomRow from "../../atoms/row";

export default function OrganismNextMeetup() {
    return (
        <>
            <div className="mt-3">
                <h4>Next Meetup</h4>

                <AtomRow>
                    <AtomCol column={11} className={'bg-secondary mx-5'}>
                        <h5>Awesome meetup and event</h5>
                        <p className="text-muted">25 Januari 2019</p>
                        <p className="fw-bold">
                            Hello, Javascript & Node.js Ninjas! <br />
                            Get ready for our monthly meetup JakartaJS! This will be our fifth meetup of 2018! <br />
                            The Meetup format will contain some short stories and technical talks. <br />
                            If you have a short announcement you'd like to share with the audience, you may do so during open mic announcements. <br /><br />
                            Remember to bring a photo ID to get through building security.
                            <br />
                            - - - - - <br />
                            See you there! <br />
                            Best, Hengki, Giovanni, Sofian, Riza, Agung The JakartaJS Organizers
                        </p>
                    </AtomCol>
                </AtomRow>
            </div>
        </>
    )
}