import OrganismAboutMeetup from "../../organisms/about-meetup";
import OrganismFooter from "../../organisms/footer";
import OrganismJumbotron from "../../organisms/jumbotron";
import OrganismNavbar from "../../organisms/navbar";
import OrganismNextMeetup from "../../organisms/next-meetup";
import OrganismPastMeetups from "../../organisms/past-meetups";

export default function TemplatesQtemu() {
    return (
        <>
            <OrganismNavbar />

            <div className="container-fluid">

                <OrganismJumbotron />

                <OrganismNextMeetup />

                <OrganismAboutMeetup />

                <OrganismPastMeetups />

                <OrganismFooter />

            </div>
        </>
    )
}