import AtomCol from "../../atoms/col";
import AtomRow from "../../atoms/row";

export default function MoleculeJumbotronData({ children, datas }) {
    return (
        <>
            <AtomRow>
                <AtomCol column={1} >
                    <p>{datas[0]}</p>
                </AtomCol>
                <AtomCol column={2} >
                    <p>{datas[1]}</p>
                </AtomCol>
            </AtomRow>

        </>
    )
}