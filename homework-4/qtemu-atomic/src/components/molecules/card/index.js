import AtomsLinkButton from "../../atoms/link/button";

export default function MoleculeCard({ children, title, body, attendance }) {
    return (
        <>
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">{title}</h5>
                    <hr />
                    <p className="card-text">
                        {body}
                    </p>
                    <p><b>{attendance}</b> went</p>
                    <AtomsLinkButton color={'primary'} route={'/'} >
                        View
                    </AtomsLinkButton>
                </div>
            </div>
        </>
    )
}