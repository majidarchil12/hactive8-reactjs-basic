import AtomCol from "../../atoms/col";
import AtomRow from "../../atoms/row";

export default function MoleculeHeaderContent({ title }) {
    return (
        <AtomRow>
            <AtomCol column={6} className={'text-start'}>
                <h5>{title}</h5>
            </AtomCol>
            <AtomCol column={6} className={'text-end'}>
                <h5>See all</h5>
            </AtomCol>
        </AtomRow>
    )
}