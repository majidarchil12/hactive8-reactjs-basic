export default function AtomCol({ children, column, className }) {
    return (
        <>
            <div className={`col-${column} ${className}`}>
                {children}
            </div >
        </>
    )
}