export default function AtomRow({ children, className }) {
    return (
        <>
            <div className={`row ${className}`}>
                {children}
            </div>
        </>
    )
}