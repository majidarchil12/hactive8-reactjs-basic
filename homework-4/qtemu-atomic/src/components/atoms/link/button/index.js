export default function AtomsLinkButton({ children, route, color }) {
    return (
        <>
            <a href={route} className={`btn btn-${color}`}>
                {children}
            </a>
        </>
    )
}