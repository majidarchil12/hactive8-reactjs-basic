export default function AtomNavLink({ route, children }) {
    return (
        <>
            <a className="nav-link" href={route}>
                {children}
            </a>
        </>
    )
}