export default function AtomsButton({ type, text, color, children }) {
    return (
        <>
            <button type={type} className={`btn btn-${color}`}>
                {children}
            </button>
        </>
    )
}